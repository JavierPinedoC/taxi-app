import Pusher from 'pusher-js';
Pusher.logToConsole = true;

var pusher = new Pusher('e6d614ae87664f06fb1b', {
    cluster: 'us2',
    forceTLS: true
});

export default pusher;