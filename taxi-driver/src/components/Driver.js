import React, { useState, useEffect } from "react";
import pusher from "../services/pusher_service";
import {
  Card,
  CardHeader,
  CardContent,
  Button,
  CardActions
} from "@material-ui/core";

function Driver(props) {
  const [pickupAddress, setPickupAddress] = useState("");
  const [dropoffAddress, setDropoffAddress] = useState("");
  const [show, setShow] = useState(false);
  const [customer, setCustomer] = useState("");

  useEffect(() => {
    var channel = pusher.subscribe(`driver_${props.username}`);
    channel.bind("booking_request", function(data) {
      setPickupAddress(data.pickup_address);
      setDropoffAddress(data.dropoff_address);
      setCustomer(data.customer_id);
      setShow(true);
    });
  }, [props]);

  const submitBookingRequest = () => {
    fetch("http://localhost:3000/api/bookings/123/accept", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        pickup_address: pickupAddress,
        dropoff_address: dropoffAddress,
        driver_id: props.username
      })
    })
      .then(res => res.json())
      .then(response => console.log(response));
  };

  return (
    <div>
      {show ? (
        <Card>
          <CardHeader title="Booking request" />
          <CardContent>
            Name of {" "}<b>{customer}</b>{" "} Trip from <b>{pickupAddress}</b> to{" "}
            <b>{dropoffAddress}</b>
          </CardContent>
          <CardActions>
            <Button
              id="accept-button"
              style={{ minWidth: "50%" }}
              onClick={() => submitBookingRequest()}
              variant="outlined"
              color="primary"
            >
              Accept
            </Button>
            <Button
              id="reject-button"
              style={{ minWidth: "50%" }}
              variant="outlined"
              color="secondary"
            >
              Reject
            </Button>
          </CardActions>
        </Card>
      ) : null}
    </div>
  );
}

export default Driver;
