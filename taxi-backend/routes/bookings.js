var express = require('express');
var fetch = require('node-fetch');
var router = express.Router();
var pusher = require('../services/pusher_service');
var h3 = require("h3-js");
var turf = require("@turf/turf");
var Bull = require('bull');

// This could be replaced to a call to the database in a real application
var taxis = {
  "viasanangel": [19.026868, -98.237389],
  "hospitalangeles": [19.022163, -98.235905],
  "orionsur": [19.0276125, -98.2539265],
  "cumulovirgo": [19.0229392, -98.2415901],
  "solesta": [19.033552, -98.229740],
  "paseodestino": [19.009594, -98.245543],
  "plazaarcangel": [19.005607, -98.267569],
  "plazalastorres": [19.0123061, -98.2467088],
  "caminoreal": [19.0152993, -98.2548204],
  "noria": [19.0334872, -98.2245549],
  "plazaarco": [19.0054526, -98.26138]
};

var index = {};

for (var id in taxis) {
  var coord = taxis[id];
  var geohash = h3.geoToH3(coord[0], coord[1], 7);
  index[geohash] = index[geohash] || [];
  index[geohash].push(id);
}

const getCandidates = function (latitude, longitude) {
  var geohash = h3.geoToH3(latitude, longitude, 7);
  // This is a known heohash with multiple possible taxis,
  // just to show the rest of the application works. Otherwise,
  // it could be possible to get a hash with no nearby drivers
  // and, therefore, return nothing
  geohash = '874994ad5ffffff';
  return index[geohash];
}

sortByDistance = function (latitude, longitude, candidates) {
  var customerPosition = turf.point([latitude, longitude]);
  return candidates
	.map( (id) => {
		var taxiPosition = turf.point(taxis[id]);
		return {
		        id: id,
		        distance: turf.distance(taxiPosition, customerPosition)
		 };
	})
	.sort((a, b) => a.distance - b.distance);
};

// Bull queue
const taxiFinderQueue = new Bull('taxi-finder');

taxiFinderQueue.process(async (request) => {
  let candidates = getCandidates(request.data.pickup.latitude, request.data.pickup.longitude).slice(0, 3);
  candidates = sortByDistance(request.data.pickup.latitude, request.data.pickup.longitude, candidates);

  console.log(candidates);

  pusher.trigger(`customer_${request.data.customer_id}`, 'booking_status', {msg: `Taxi candidates found`, candidates: candidates});
});


// Take an collection of objects, each with a "latitude" and a "longitude" property
// and format a string in the format requested by the Mapbox matrix API.
const sanitizeCoordinates = (objs) => {
  let result = [];

  for(obj of objs) {
    result.push(`${obj.longitude},${obj.latitude}`)
  }

  return result.join(';');
};



router.post('/', function(req, res, next) {
  console.log(req.body);
  
  pusher.trigger('driver_pedro', 'booking_request', req.body);

  // In theory, booking should be equal to the payload delivered by the client frontend.
  // It works, as can be seen by the console.log() on the top. But for the purposes
  // of the excercise, will override it with the exercise-requested direction:
  //let booking = req.body
  let booking = {
    customer_id: req.body.customer_id,
    pickup: {
      address: 'Tecnologico de Monterrey campus Puebla',
      latitude: 19.0184421,
      longitude: -98.2444165
    },

    dropoff: req.body.dropoff,
  }
  console.log(booking);

  taxiFinderQueue.add(booking);

  res.send({msg: 'Estamos procesando su petición'});
});

router.post('/123/accept', function(req, res, next) {
  // console.log("Si lo encuentra");
  console.log(req.body);
  // System should:
  // - Find near-by taxis
  // - Select the one that is closest to the pickup address
  // - Contact such taxi, proposing him/her to take the ride
  // ----------------------------------
  // In this moment, the system always contacts "pedro"
  pusher.trigger('customer_javier', 'booking_status', req.body);
  res.send({msg: 'We are processing your booking driver'});
});
module.exports = router;
