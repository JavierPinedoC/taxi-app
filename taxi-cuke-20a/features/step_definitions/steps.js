const {Given, When, Then, After, Before} = require('cucumber');
const puppeteer = require('puppeteer');
const assert = require('assert');

var browser;
var customerPage;
var pedroPage;
var gpickup_address, gdropoff_address;

Before(async function() {
    browser = await puppeteer.launch({headless: false});
});

After(async function() {
    // await browser.close();
});


Given('the following taxis are on duty', async function (dataTable) {
  pedroPage = await browser.newPage();
  await pedroPage.goto('http://localhost:5000/');
});

Given('I want to go from {string} to {string}', function (pickup_address, dropoff_address) {
  gpickup_address = pickup_address;
  gdropoff_address = dropoff_address;
});

Given('I open the application\'s web page', async function () {
  customerPage = await browser.newPage();
  await customerPage.goto('http://localhost:4000/');
});

Given('I enter the booking information', async function () {
  await customerPage.focus('#pickup-address');
  await customerPage.keyboard.type(gpickup_address);
  await customerPage.focus('#dropoff-address');
  await customerPage.keyboard.type(gdropoff_address);
});

When('I summit the booking request', async function () {
  await customerPage.focus('#submit-button');
  await customerPage.keyboard.press('Enter', {delay: 1000});
});

Then('I should receive a confirmation message', async function () {
  assert(await customerPage.evaluate(() => window.find("We are processing your request")));
});

Then('{string} should receive a ride request', async function (string) {
  assert(await pedroPage.evaluate(() => window.find("Trip")))
});

When('{string} accepts the ride request', async function (string) {
  await pedroPage.focus('#accept-button');
  await pedroPage.keyboard.press('Enter', {delay: 1000});
});

Then('I should be notified {string} accepted the request', async function (string) {
  assert(await customerPage.evaluate(() => window.find("Pedro accepted your travel")));
});