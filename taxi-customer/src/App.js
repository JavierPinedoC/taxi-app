import React from 'react';
import './App.css';

import Customer from './components/Customer';

function App() {
  return (
    <div className="App">
      <p/>
      <Customer username = "javier" />
    </div>
  );
}

export default App;