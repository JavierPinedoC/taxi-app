import React, { useState, useEffect } from "react";
import pusher from "../services/pusher_service";
import { TextField, Button } from "@material-ui/core";
import Swal from 'sweetalert2'
import MapGL, {Marker} from 'react-map-gl';
import PersonPinCircleIcon from '@material-ui/icons/PersonPinCircle';

function Customer(props) {
  const [pickupAddress, setPickupAddress] = useState("");
  const [dropoffAddress, setDropoffAddress] = useState("");
  const [show, setShow] = useState(false);

  var [PickupLatitude, setPickupLatitude] = useState(0);
  var [PickupLongitude, setPickupLongitude] = useState(0);
  var [DropoffLatitude, setDropoffLatitude] = useState(0);
  var [DropoffLongitude, setDropoffLongitude] = useState(0);

  var [viewPortState, setViewPortState] = useState({
    viewport: {
      latitude: 37.78,
      longitude: -122.41,
      zoom: 14
    }
  });

  useEffect(() => {
    var channel = pusher.subscribe(`customer_${props.username}`);
    channel.bind("booking_status", function(data) {
      Swal.fire(
        'Hola',
        data.msg,
        'info'
      );
    });
  }, [props]);


  const reverseGeocode = () => {
    navigator.geolocation.getCurrentPosition((location) => {
      setPickupLongitude(location.coords.longitude);
      setPickupLatitude(location.coords.latitude);
      let viewport = viewPortState.viewport;
      viewport.latitude = PickupLatitude;
      viewport.longitude = PickupLongitude;
      setViewPortState({viewport});

    });

    fetch(`https://api.mapbox.com/geocoding/v5/mapbox.places/${PickupLongitude},${PickupLatitude}.json?access_token=${process.env.REACT_APP_MAPBOX_TOKEN}`)
    .then((res) => {return res.json()})
    .then((res) => {
      if(res.features &&  res.features.length > 0) {
        setPickupAddress(res.features[0].place_name);
      }
    });
  }

  const urlSanitize = (str) => {
    return encodeURI(str.replace(";", "")); // Mapbox doesn't like semicolons
  }

  const forwardGeocode = () => {
    fetch(`https://api.mapbox.com/geocoding/v5/mapbox.places/${urlSanitize(dropoffAddress)}.json?access_token=${process.env.REACT_APP_MAPBOX_TOKEN}&?proximity=${PickupLongitude},${PickupLatitude}`)
    .then((res) => res.json())
    .then((res) => {
      if(res.features &&  res.features.length > 0) {
        let name = res.features[0].place_name;
        let coords = res.features[0].geometry.coordinates;

        setDropoffAddress(name);
        setDropoffLongitude(coords[0]);
        setDropoffLatitude(coords[1]);
      }
    });
  }


  const submitBookingRequest = () => {
    fetch("http://localhost:3000/api/bookings", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({

        pickup: {
          address: pickupAddress,
          longitude: PickupLongitude,
          latitude: PickupLatitude
        },

        dropoff: {
          address: dropoffAddress,
          longitude: DropoffLongitude,
          latitude: DropoffLatitude
        },

        customer_id: props.username
      })
    })
      .then(res => res.json())
      .then(response => {
        console.log(response);
        Swal.fire(
        'Request',
        response.msg,
        'info')
    });
  };

  const mystlye = {
    minWidth: "50%",
    minHeight: 50,
    marginTop: "20px"
  };

  return (
    <div>
      <div>
      <Button
          id="update-pickup-button"
          onClick={() => {
              reverseGeocode();
            }
          }
          variant="contained"
          size="small"
          color="primary"
        >Update Location</Button>
        <br/>
        <TextField
          id="pickup-address"
          value={pickupAddress}
          onChange={ev => {
              setPickupAddress(ev.target.value);
            }
          }
          label="Pickup Address"
          variant="outlined"
          style={mystlye}
        />
      </div>
      <div>
        <TextField
          id="dropoff-address"
          value={dropoffAddress}
          onChange={ev => {
              setDropoffAddress(ev.target.value);
            }
          }
          label="Dropoff address"
          variant="outlined"
          style={mystlye}
        />
        <br/>
        <Button
          id="update-dropoff-button"
          onClick={() => {
              forwardGeocode();
            }
          }
          variant="contained"
          size="small"
          color="primary"
          //style={mystlye}
        >Search for dropoff address</Button>
      </div>
      <div>
        <p/>
        <Button
          id="submit-button"
          onClick={() => {
            submitBookingRequest();
          }}
          variant="outlined"
          size="large"
          color="primary"
          style={mystlye}
        >
          Submit request
        </Button>
      </div>
      <div>
      <MapGL
        latitude={viewPortState.viewport.latitude} longitude={viewPortState.viewport.longitude}
        width="100%" height={600} zoom={viewPortState.viewport.zoom}
        mapboxApiAccessToken={process.env.REACT_APP_MAPBOX_TOKEN}
        style={mystlye}
        onViewportChange={(viewport) => setViewPortState({ viewport })}
        >
        
        { [{latitude: PickupLatitude, longitude: PickupLongitude}, {latitude: DropoffLatitude, longitude: DropoffLongitude}]
        .filter((coords) => coords.latitude !== 0 && coords.longitude !== 0)
        .map( (coords) => {
          return (
            <Marker
            latitude={coords.latitude} longitude={coords.longitude}
            offsetLeft={-10} offsetTop={-20}>
            <PersonPinCircleIcon/>
          </Marker>
          );
        }) }
      </MapGL>
    </div>

      {show ? (
        Swal.fire(
          'Pedro',
          'Pedro accepted your travel',
          'info'
        ),
        <TextField disabled/>
      ) : null}
    </div>
  );
}

export default Customer;
